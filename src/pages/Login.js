import { useState, useEffect, useContext } from 'react';
import { Container, Form, Button } from 'react-bootstrap';
import UserContext from '../UserContaxt';
import Swal from 'sweetalert2';
import { Redirect } from 'react-router-dom';

export default function Login() {
    const { user, setUser, redirect } = useContext(UserContext);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const [isDisabled, setIsDisabled] = useState(true);

    useEffect(() => {
        redirect(user);
        if (email !== '' && password !== '') {
            setIsDisabled(false);
        } else {
            setIsDisabled(true);
        }
    }, [email, password]);

    function Login(e) {
        e.preventDefault();
        fetch('http://localhost:4000/api/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: email,
                password: password,
            }),
        })
            .then((res) => res.json())
            .then((data) => {
                if (data) {
                    localStorage.setItem('token', data.access);
                    userDetails(data.access);
                    Swal.fire({
                        title: 'Login Successful',
                        icon: 'success',
                        text: 'Welcome to course booking!',
                    });
                } else {
                    Swal.fire({
                        title: 'Login failed',
                        icon: 'error',
                        text: 'Invalid username or password',
                    });
                }
            });
        setEmail('');
        setPassword('');
    }

    const userDetails = (token) => {
        fetch('http://localhost:4000/api/users/details', {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        })
            .then((res) => res.json())
            .then((data) => {
                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin,
                });
            });
    };

    return user.id ? (
        <Redirect to="/courses" />
    ) : (
        <Container>
            <Form className="border p-3 mb-3" onSubmit={(e) => Login(e)}>
                {/*email*/}
                <Form.Group className="mb-3" controlId="email">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control
                        type="email"
                        placeholder="Enter email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                    />
                </Form.Group>
                {/*password*/}
                <Form.Group className="mb-3" controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                    />
                </Form.Group>
                <Button variant="primary" type="submit" disabled={isDisabled}>
                    Submit
                </Button>
            </Form>
        </Container>
    );
}
