import { useContext, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import UserContext from '../UserContaxt';

export default function Logout() {
    const { unsetUser, setUser } = useContext(UserContext);
    unsetUser();
    useEffect(() => {
        setUser({ email: null });
    }, []);
    return <Redirect to="/login" />;
}
