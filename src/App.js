import { useState } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import { UserProvider } from './UserContaxt';

import 'bootstrap/dist/css/bootstrap.min.css';

/*components*/
import AppNavbar from './components/AppNavbar';

/*pages*/
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './components/Error';

export default function App() {
    const [user, setUser] = useState({
        id: null,
        isAdmin: null,
    });

    const unsetUser = () => {
        localStorage.clear();
    };

    const redirect = (user) => {
        if (user) {
            <Redirect to="/courses" />;
        }
    };

    return (
        <UserProvider value={{ user, setUser, unsetUser, redirect }}>
            <BrowserRouter>
                <AppNavbar />
                <Container fluid className="m-3">
                    <Switch>
                        <Route exact path="/" component={Home} />
                        <Route exact path="/courses" component={Courses} />
                        <Route exact path="/register" component={Register} />
                        <Route exact path="/login" component={Login} />
                        <Route exact path="/logout" component={Logout} />
                        <Route exact path="*" component={Error} />
                    </Switch>
                </Container>
            </BrowserRouter>
        </UserProvider>
    );
}
