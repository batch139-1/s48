import { useState, Fragment, useContext } from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContaxt';
import Logout from '../pages/Logout';

export default function AppNavbar() {
    // const [user, setUser] = useState(localStorage.getItem('email'));
    const { user } = useContext(UserContext);

    let leftNav =
        user.email != null ? (
            <Nav.Link as={NavLink} to="/logout">
                Logout
            </Nav.Link>
        ) : (
            <Fragment>
                <Nav.Link as={NavLink} to="/register">
                    Register
                </Nav.Link>
                <Nav.Link as={NavLink} to="/login">
                    Login
                </Nav.Link>
            </Fragment>
        );

    return (
        <Navbar bg="primary" expand="lg">
            <Navbar.Brand as={Link} to="/">
                Course Booking
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="me-auto">
                    <Nav.Link as={NavLink} to="/">
                        Home
                    </Nav.Link>
                    <Nav.Link as={NavLink} to="/courses">
                        Courses
                    </Nav.Link>
                    {/*
              <Nav.Link >Register</Nav.Link>
              <Nav.Link >Login</Nav.Link>
              <Nav.Link >Logout</Nav.Link>
            */}
                </Nav>
                <Nav className="me-auto">{leftNav}</Nav>
            </Navbar.Collapse>
        </Navbar>
    );
}
