import React from 'react';
import Banner from './Banner';

const Error = () => {
    const data = {
        title: '404 - Not found',
        content: 'The Page you are looking for is not found',
        destination: '/',
        label: 'Back to homepage',
    };

    return <Banner data={data} />;
};

export default Error;
